package dz.telegramutils.service.dev;

import dz.telegramutils.dto.ContactFilters;
import dz.telegramutils.dto.TelegramUser;
import dz.telegramutils.model.data.GenerateDevData;
import dz.telegramutils.service.ContactsService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile("dev")
public class ContactsServiceDev implements ContactsService {

    @Override
    public List<TelegramUser> getAllContacts(String sessionId) {
        return GenerateDevData.getTelegramUsers();
    }

    @Override
    public List<ContactFilters> getContactFilters(String sessionId, Integer currentId) {
        return GenerateDevData.getContactFilters();
    }

}
