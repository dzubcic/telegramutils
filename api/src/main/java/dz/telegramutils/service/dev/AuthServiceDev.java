package dz.telegramutils.service.dev;

import dz.telegramutils.dto.SessionResponse;
import dz.telegramutils.dto.TelegramUser;
import dz.telegramutils.model.data.GenerateDevData;
import dz.telegramutils.service.AuthService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("dev")
public class AuthServiceDev implements AuthService {

    @Override
    public SessionResponse checkPhoneNumber(String phoneNumber, String sessionId) {
        return new SessionResponse("123456789");
    }

    @Override
    public TelegramUser checkAuthCode(String sessionId, String authCode) {
        return GenerateDevData.getTelegramUsers().get(0);
    }

    @Override
    public TelegramUser getCurrentUser(String sessionId) {
        return GenerateDevData.getTelegramUsers().get(0);
    }

    @Override
    public void logout(String sessionId) {

    }

}
