package dz.telegramutils.service.dev;

import dz.telegramutils.dto.TelegramUser;
import dz.telegramutils.model.data.GenerateDevData;
import dz.telegramutils.service.ExportService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;

@Service
@Profile("dev")
public class ExportServiceDev implements ExportService {

    public String exportCSV(String sessionId, Set<Integer> contactIds) {
        List<TelegramUser> contacts = GenerateDevData.getTelegramUsers().subList(0, contactIds.size());
        List<String> csvRows = new ArrayList<>();
        csvRows.add(new StringJoiner(";").add("FirstName").add("LastName").add("PhoneNumber").toString());
        contacts.forEach(c -> csvRows.add(new StringJoiner(";").add(c.getFirstName()).add(c.getLastName()).add(c.getPhoneNumber()).toString()));
        return String.join("\n", csvRows);
    }

    public String exportVCARD(String sessionId, Set<Integer> contactIds) {
        List<TelegramUser> contacts = GenerateDevData.getTelegramUsers().subList(0, contactIds.size());
        List<String> rows = new ArrayList<>();
        contacts.forEach(c -> {
            rows.add("BEGIN:VCARD");
            rows.add("VERSION:4.0");
            rows.add("N:" + c.getLastName() + ";" + c.getFirstName() + ";;;");
            rows.add("FN:" + c.getFirstName() + " " + c.getLastName());
            rows.add("TEL;TYPE=voice,work,pref,cell:" + c.getPhoneNumber());
            rows.add("END:VCARD");
        });
        return String.join("\n", rows);
    }

}
