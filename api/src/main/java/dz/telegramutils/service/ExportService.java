package dz.telegramutils.service;

import java.util.Set;

public interface ExportService {

    public String exportCSV(String sessionId, Set<Integer> contactIds);

    public String exportVCARD(String sessionId, Set<Integer> contactIds);

}