package dz.telegramutils.service;

import dz.telegramutils.dto.SessionResponse;
import dz.telegramutils.dto.TelegramUser;

import java.io.IOException;

public interface AuthService {

    SessionResponse checkPhoneNumber(String phoneNumber, String sessionId) throws IOException;

    TelegramUser checkAuthCode(String sessionId, String authCode);

    TelegramUser getCurrentUser(String sessionId);

    void logout(String sessionId) throws IOException;

}
