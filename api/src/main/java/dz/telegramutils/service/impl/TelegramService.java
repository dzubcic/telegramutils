package dz.telegramutils.service.impl;

import dz.telegramutils.dto.TelegramException;
import dz.telegramutils.dto.TelegramUser;
import dz.telegramutils.dto.mapper.TelegramUserMapper;
import dz.telegramutils.model.Client;
import dz.telegramutils.model.TdApi;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class TelegramService {
    public static final String TDLIBDB = "tdlib-db/";

    TdApi.Object execute(Client client, TdApi.Function toExecute) {
        return execute(client, toExecute, true);
    }

    TdApi.Object executeIgnoreError(Client client, TdApi.Function toExecute) {
        return execute(client, toExecute, false);
    }

    Client createClient(String sessionId) {
        Client client = Client.create(null, null, null);
        client.send(new TdApi.SetTdlibParameters(getTDLibParameters(sessionId)), null);
        client.send(new TdApi.CheckDatabaseEncryptionKey(), null);
        return client;
    }

    private TdApi.Object execute(Client client, TdApi.Function toExecute, boolean handleError) {
        AtomicReference<TdApi.Object> objectAtomicReference = new AtomicReference<>();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        client.send(toExecute, object -> {
            objectAtomicReference.set(object);
            countDownLatch.countDown();
        });
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        TdApi.Object object = objectAtomicReference.get();
        if (handleError && isError(object)) {
            TdApi.Error error = (TdApi.Error) object;
            throw new TelegramException(error.code, error.message);
        }
        return object;
    }

    List<TelegramUser> getUsersByIds(Client client, Collection<Integer> ids, boolean resolvePhoto) {
        return ids.stream().map(id -> {
            TdApi.User contact = (TdApi.User) execute(client, new TdApi.GetUser(id));
            TelegramUser telegramUser = TelegramUserMapper.mapFromEntity(contact);
            if (resolvePhoto) {
                telegramUser.setProfilePhoto(resolveProfilePhoto(client, contact));
            }
            return telegramUser;
        }).collect(Collectors.toList());
    }

    String resolveProfilePhoto(Client client, TdApi.User user) {
        if (user.profilePhoto != null) {
            if (!user.profilePhoto.small.local.isDownloadingCompleted) {
                TdApi.File remoteFile = (TdApi.File) execute(client, new TdApi.GetRemoteFile(user.profilePhoto.small.remote.id, new TdApi.FileTypeProfilePhoto()));
                execute(client, new TdApi.DownloadFile(remoteFile.id, 32));
                while (true) {
                    TdApi.File file = (TdApi.File) execute(client, new TdApi.GetRemoteFile(user.profilePhoto.small.remote.id, new TdApi.FileTypeProfilePhoto()));
                    if (file.local.isDownloadingCompleted) {
                        return file.local.path;
                    }
                    try {
                        Thread.sleep(30);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            else {
                return user.profilePhoto.small.local.path;
            }
        }
        return null;
    }

    boolean isError(TdApi.Object object) {
        return object.getConstructor() == TdApi.Error.CONSTRUCTOR;
    }

    private TdApi.TdlibParameters getTDLibParameters(String sessionId) {
        TdApi.TdlibParameters parameters = new TdApi.TdlibParameters();
        parameters.databaseDirectory = TDLIBDB + sessionId;
        parameters.useMessageDatabase = true;
        parameters.useSecretChats = true;
        parameters.apiId = 902821;
        parameters.apiHash = "89005bc46fccd44bcd1a1900d476c4ee";
        parameters.systemLanguageCode = "en";
        parameters.deviceModel = "Desktop";
        parameters.systemVersion = "Unknown";
        parameters.applicationVersion = "1.0";
        parameters.enableStorageOptimizer = true;
        return parameters;
    }

}
