package dz.telegramutils.service.impl;

import dz.telegramutils.dto.ContactFilterTypes;
import dz.telegramutils.dto.ContactFilters;
import dz.telegramutils.dto.TelegramUser;
import dz.telegramutils.model.Client;
import dz.telegramutils.model.TdApi;
import dz.telegramutils.service.ContactsService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Profile("!dev")
public class ContactsServiceImpl extends TelegramService implements ContactsService {

    public List<TelegramUser> getAllContacts(String sessionId) {
        Client client = createClient(sessionId);
        TdApi.Users contactsIds = (TdApi.Users) execute(client, new TdApi.GetContacts());
        List<Integer> contactsIdsList = Arrays.stream(contactsIds.userIds).skip(1).boxed().collect(Collectors.toList());
        return getUsersByIds(client, contactsIdsList, true);
    }

    public List<ContactFilters> getContactFilters(String sessionId, Integer currentId) {
        Client client = createClient(sessionId);
        List<ContactFilters> contactFilters = new ArrayList<>();
        ContactFilters groupFilters = new ContactFilters(ContactFilterTypes.GROUP);
        ContactFilters superGroupFilters = new ContactFilters(ContactFilterTypes.SUPERGROUP);
        ContactFilters channelFilters = new ContactFilters(ContactFilterTypes.CHANNEL);

        TdApi.Chats chatIds = (TdApi.Chats) execute(client, new TdApi.GetChats(9223372036854775807L, 0, 100));
        Arrays.stream(chatIds.chatIds).forEach(c -> {
            TdApi.Chat chat = (TdApi.Chat) execute(client, new TdApi.GetChat(c));
            if (chat.type instanceof TdApi.ChatTypeBasicGroup) {
                TdApi.BasicGroupFullInfo groupFullInfo = (TdApi.BasicGroupFullInfo) execute(client, new TdApi.GetBasicGroupFullInfo(((TdApi.ChatTypeBasicGroup) chat.type).basicGroupId));
                List<Integer> userIds = Arrays.stream(groupFullInfo.members).filter(m -> m.userId != currentId).map(m -> m.userId).collect(Collectors.toList());
                groupFilters.getFilters().add(new ContactFilters.Filter(chat.title, userIds));
            }
            else if (chat.type instanceof TdApi.ChatTypeSupergroup) {
                TdApi.Object object = executeIgnoreError(client, new TdApi.GetSupergroupMembers(((TdApi.ChatTypeSupergroup) chat.type).supergroupId, null, 0, 100));
                if (!isError(object)) {
                    TdApi.ChatMembers chatMembers = (TdApi.ChatMembers) object;
                    List<Integer> userIds = Arrays.stream(chatMembers.members).filter(m -> m.userId != currentId).map(m -> m.userId).collect(Collectors.toList());
                    if (((TdApi.ChatTypeSupergroup) chat.type).isChannel) {
                        channelFilters.getFilters().add(new ContactFilters.Filter(chat.title, userIds));
                    }
                    else {
                        superGroupFilters.getFilters().add(new ContactFilters.Filter(chat.title, userIds));
                    }
                }
            }
        });

        contactFilters.add(groupFilters);
        contactFilters.add(superGroupFilters);
        contactFilters.add(channelFilters);
        return contactFilters;
    }

}
