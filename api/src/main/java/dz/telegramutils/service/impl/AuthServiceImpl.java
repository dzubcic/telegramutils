package dz.telegramutils.service.impl;

import dz.telegramutils.dto.SessionResponse;
import dz.telegramutils.dto.TelegramException;
import dz.telegramutils.dto.TelegramUser;
import dz.telegramutils.dto.mapper.TelegramUserMapper;
import dz.telegramutils.model.Client;
import dz.telegramutils.model.TdApi;
import dz.telegramutils.service.AuthService;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Service
@Profile("!dev")
public class AuthServiceImpl extends TelegramService implements AuthService {

    public SessionResponse checkPhoneNumber(String phoneNumber, String sessionId) throws IOException {
        Client client = createClient(sessionId);
        TdApi.Object object = executeIgnoreError(client, new TdApi.SetAuthenticationPhoneNumber(phoneNumber, false, false));
        if (isError(object)) {
            File file = new File(TDLIBDB + sessionId);
            FileUtils.deleteDirectory(file);
            TdApi.Error error = (TdApi.Error) object;
            throw new TelegramException(error.code, error.message);
        }
        return new SessionResponse(sessionId);
    }

    public TelegramUser checkAuthCode(String sessionId, String authCode) {
        Client client = createClient(sessionId);
        execute(client, new TdApi.CheckAuthenticationCode(authCode, null, null));
        return getCurrentUser(client);
    }

    public TelegramUser getCurrentUser(String sessionId) {
        Client client = createClient(sessionId);
        return getCurrentUser(client);
    }

    public void logout(String sessionId) throws IOException {
        Client client = createClient(sessionId);
        execute(client, new TdApi.LogOut());
        File file = new File(TDLIBDB + sessionId);
        FileUtils.deleteDirectory(file);
    }

    private TelegramUser getCurrentUser(Client client) {
        TdApi.User user = (TdApi.User) execute(client, new TdApi.GetMe());
        TelegramUser telegramUser = TelegramUserMapper.mapFromEntity(user);
        telegramUser.setProfilePhoto(resolveProfilePhoto(client, user));
        return telegramUser;
    }

}
