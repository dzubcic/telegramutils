package dz.telegramutils.service.impl;

import dz.telegramutils.dto.TelegramUser;
import dz.telegramutils.model.Client;
import dz.telegramutils.service.ExportService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;

@Service
@Profile("!dev")
public class ExportServiceImpl extends TelegramService implements ExportService {
    private static final String DELIMITER = ";";

    public String exportCSV(String sessionId, Set<Integer> contactIds) {
        Client client = createClient(sessionId);
        List<TelegramUser> contacts = getUsersByIds(client, contactIds, false);
        List<String> csvRows = new ArrayList<>();
        csvRows.add(new StringJoiner(DELIMITER).add("FirstName").add("LastName").add("PhoneNumber").toString());
        contacts.forEach(c -> csvRows.add(new StringJoiner(DELIMITER).add(c.getFirstName()).add(c.getLastName()).add(c.getPhoneNumber()).toString()));
        return String.join("\n", csvRows);
    }

    public String exportVCARD(String sessionId, Set<Integer> contactIds) {
        Client client = createClient(sessionId);
        List<TelegramUser> contacts = getUsersByIds(client, contactIds, false);
        List<String> rows = new ArrayList<>();
        contacts.forEach(c -> {
            rows.add("BEGIN:VCARD");
            rows.add("VERSION:4.0");
            rows.add("N:" + c.getLastName() + ";" + c.getFirstName() + ";;;");
            rows.add("FN:" + c.getFirstName() + " " + c.getLastName());
            rows.add("TEL;TYPE=voice,work,pref,cell:" + c.getPhoneNumber());
            rows.add("END:VCARD");
        });
        return String.join("\n", rows);
    }

}
