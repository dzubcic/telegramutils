package dz.telegramutils.service;

import dz.telegramutils.dto.ContactFilters;
import dz.telegramutils.dto.TelegramUser;

import java.util.List;

public interface ContactsService {

    List<TelegramUser> getAllContacts(String sessionId);

    List<ContactFilters> getContactFilters(String sessionId, Integer currentId);

}
