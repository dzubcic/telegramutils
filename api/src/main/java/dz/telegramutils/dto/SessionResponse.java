package dz.telegramutils.dto;

public class SessionResponse {
    private String sessionId;

    public SessionResponse(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }
}
