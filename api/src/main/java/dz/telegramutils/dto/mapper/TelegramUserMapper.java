package dz.telegramutils.dto.mapper;

import dz.telegramutils.dto.TelegramUser;
import dz.telegramutils.model.TdApi;

public class TelegramUserMapper {

    public static TelegramUser mapFromEntity(TdApi.User user) {
        TelegramUser telegramUser = new TelegramUser();
        telegramUser.setId(user.id);
        telegramUser.setFirstName(user.firstName);
        telegramUser.setLastName(user.lastName);
        telegramUser.setPhoneNumber(user.phoneNumber);
        return telegramUser;
    }

}
