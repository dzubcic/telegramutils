package dz.telegramutils.dto;

import java.util.ArrayList;
import java.util.List;

public class ContactFilters {
    private ContactFilterTypes filterType;
    private List<Filter> filters;

    public ContactFilters(ContactFilterTypes filterType) {
        this.filterType = filterType;
        this.filters = new ArrayList<>();
    }

    public ContactFilterTypes getFilterType() {
        return filterType;
    }

    public void setFilterType(ContactFilterTypes filterType) {
        this.filterType = filterType;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    public static class Filter {
        private String title;
        private List<Integer> memberIds;

        public Filter(String title, List<Integer> memberIds) {
            this.title = title;
            this.memberIds = memberIds;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public List<Integer> getMemberIds() {
            return memberIds;
        }

        public void setMemberIds(List<Integer> memberIds) {
            this.memberIds = memberIds;
        }
    }
}
