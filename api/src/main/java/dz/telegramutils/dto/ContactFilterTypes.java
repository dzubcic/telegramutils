package dz.telegramutils.dto;

public enum ContactFilterTypes {
    GROUP, SUPERGROUP, CHANNEL
}
