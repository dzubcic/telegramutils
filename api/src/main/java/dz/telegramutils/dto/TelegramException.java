package dz.telegramutils.dto;

public class TelegramException extends RuntimeException {
    private int code;
    private String message;

    public TelegramException(int code, String message) {
        super(message);
        this.message = message;
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
