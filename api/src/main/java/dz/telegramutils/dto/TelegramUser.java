package dz.telegramutils.dto;

import dz.telegramutils.service.impl.TelegramService;

public class TelegramUser {
    private int id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String profilePhoto;

    public TelegramUser() {}

    public TelegramUser(int id, String firstName, String lastName, String phoneNumber, String profilePhoto) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.profilePhoto = profilePhoto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return "+" + phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        if (profilePhoto != null) {
            this.profilePhoto = profilePhoto.substring(profilePhoto.indexOf(TelegramService.TDLIBDB)).replace(TelegramService.TDLIBDB, "image/");
        }
    }

    @Override
    public String toString() {
        return "TelegramUser{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
