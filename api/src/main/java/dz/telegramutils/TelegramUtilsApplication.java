package dz.telegramutils;

import dz.telegramutils.model.Log;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOError;
import java.io.IOException;

@SpringBootApplication
public class TelegramUtilsApplication {

    static {
        System.loadLibrary("tdjni");
    }

    public static void main(String[] args) {
        SpringApplication.run(TelegramUtilsApplication.class, args);
        Log.setVerbosityLevel(0);
        if (!Log.setFilePath("tdlib.log")) {
            throw new IOError(new IOException("Write access to the current directory is required"));
        }
    }

}