package dz.telegramutils.bot.model;

import javax.persistence.*;

@Entity
@Table
public class Step {

    @Id
    @Column
    private long id;

    @Column
    private String message;

    public Step() {}

    public Step(long id, String message) {
        this.id = id;
        this.message = message;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
