package dz.telegramutils.bot.model;

public enum BotInformation {
    INIT_MESSAGE, END_MESSAGE, EMAIL
}
