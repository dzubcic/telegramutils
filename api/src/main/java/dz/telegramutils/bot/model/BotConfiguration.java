package dz.telegramutils.bot.model;

import java.util.List;

public class BotConfiguration {

    private String initMessage;
    private String endMessage;
    private String email;
    private List<String> botSteps;

    public String getInitMessage() {
        return initMessage;
    }

    public void setInitMessage(String initMessage) {
        this.initMessage = initMessage;
    }

    public String getEndMessage() {
        return endMessage;
    }

    public void setEndMessage(String endMessage) {
        this.endMessage = endMessage;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getBotSteps() {
        return botSteps;
    }

    public void setBotSteps(List<String> botSteps) {
        this.botSteps = botSteps;
    }
}
