package dz.telegramutils.bot.model;

import javax.persistence.*;

@Entity
@Table
public class Info {

    @Id
    @Column
    private BotInformation name;

    @Column
    private String value;

    public Info() {}

    public Info(BotInformation name, String value) {
        this.name = name;
        this.value = value;
    }

    public BotInformation getName() {
        return name;
    }

    public void setName(BotInformation name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
