package dz.telegramutils.bot.model;

import javax.persistence.*;

@Entity
@Table
public class User {

    @Id
    @Column
    private long chatId;

    @Column
    private String firstName;

    @Column
    private String lastName;

    public long getChatId() {
        return chatId;
    }

    public void setChatId(long chatId) {
        this.chatId = chatId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
