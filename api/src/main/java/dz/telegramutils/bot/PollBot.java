package dz.telegramutils.bot;

import dz.telegramutils.bot.service.BotService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

import javax.annotation.PostConstruct;

@Component
@Profile("!dev")
public class PollBot extends TelegramLongPollingBot {

    private final BotService botService;

    public PollBot(BotService botService) {
        this.botService = botService;
    }

    static {
        ApiContextInitializer.init();
    }

    @PostConstruct
    public void registerBot() {
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(this);
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpdateReceived(Update update) {
        long chatId = update.getMessage().getChatId();
        botService.resolveUpdate(update.getMessage()).forEach(message -> {
            SendMessage sendMessage = new SendMessage()
                    .setChatId(chatId)
                    .setText(message);
            try {
                execute(sendMessage);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public String getBotUsername() {
        return "DZPollBot";
    }

    @Override
    public String getBotToken() {
        return "979877552:AAEDyKsqUiAIkxbzU2c-SOIv_nZ72iowUuA";
    }

}