package dz.telegramutils.bot.repository;

import dz.telegramutils.bot.model.User;
import dz.telegramutils.bot.model.Workflow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface WorkflowRepository extends JpaRepository<Workflow, Long> {

    Workflow findByUser(User user);

    void deleteByUser(User user);

}
