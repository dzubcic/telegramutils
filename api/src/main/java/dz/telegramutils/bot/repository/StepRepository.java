package dz.telegramutils.bot.repository;

import dz.telegramutils.bot.model.Step;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface StepRepository extends JpaRepository<Step, Long> {
}
