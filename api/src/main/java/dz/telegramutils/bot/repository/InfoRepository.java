package dz.telegramutils.bot.repository;

import dz.telegramutils.bot.model.Info;
import dz.telegramutils.bot.model.BotInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InfoRepository extends JpaRepository<Info, Integer> {

    Info findByName(BotInformation name);

}
