package dz.telegramutils.bot.repository;

import dz.telegramutils.bot.model.Response;
import dz.telegramutils.bot.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ResponseRepository extends JpaRepository<Response, Long> {

    List<Response> findAllByUser(User user);

    void deleteAllByUser(User user);

}
