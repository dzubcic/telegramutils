package dz.telegramutils.bot.controller;

import dz.telegramutils.bot.model.BotConfiguration;
import dz.telegramutils.bot.model.BotInformation;
import dz.telegramutils.bot.model.Step;
import dz.telegramutils.bot.service.InfoService;
import dz.telegramutils.bot.service.StepService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@RequestMapping("/bot")
public class BotController {
    private final Logger logger = LoggerFactory.getLogger(BotController.class);

    private final InfoService infoService;
    private final StepService stepService;

    public BotController(InfoService infoService, StepService stepService) {
        this.infoService = infoService;
        this.stepService = stepService;
    }

    @GetMapping
    public ResponseEntity<BotConfiguration> getConfiguration() {
        BotConfiguration botConfiguration = new BotConfiguration();
        botConfiguration.setInitMessage(infoService.getBotInformation(BotInformation.INIT_MESSAGE));
        botConfiguration.setEndMessage(infoService.getBotInformation(BotInformation.END_MESSAGE));
        botConfiguration.setEmail(infoService.getBotInformation(BotInformation.EMAIL));
        botConfiguration.setBotSteps(stepService.getAll().stream().map(Step::getMessage).collect(Collectors.toList()));
        logger.info("Getting current bot configuration: {}", botConfiguration);
        return new ResponseEntity<>(botConfiguration, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity configureBot(@RequestBody BotConfiguration botConfiguration) {
        logger.info("Saving new bot configuration: {}", botConfiguration);
        infoService.saveBotInformation(botConfiguration);
        stepService.saveSteps(botConfiguration.getBotSteps());
        return new ResponseEntity(HttpStatus.OK);
    }

}
