package dz.telegramutils.bot.service;

import dz.telegramutils.bot.model.Response;
import dz.telegramutils.bot.model.Step;
import dz.telegramutils.bot.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.StringJoiner;

@Service
public class MailService {
    private final Logger logger = LoggerFactory.getLogger(MailService.class);
    private final JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String sender;

    public MailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    void sendResponseEmail(User user, String recipient, List<Response> responses, List<Step> steps) {
        logger.info("Sending response mail from {} to {}, with responses {} for steps {}", user, recipient, responses, steps);
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);

        String htmlContent = constructMessageHTML(user, responses, steps);

        try {
            helper.setTo(recipient);
            helper.setFrom(new InternetAddress(sender, "DZPoll Bot"));
            helper.setSubject("Poll Bot response");
            helper.setText(htmlContent, true);
        } catch (MessagingException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        javaMailSender.send(helper.getMimeMessage());
        logger.info("Success sending response mail from {} to {}, with responses {} for steps {}", user, recipient, responses, steps);
    }

    private String constructMessageHTML(User user, List<Response> responses, List<Step> steps) {
        StringJoiner htmlContent = new StringJoiner("")
                .add("<p style='color: #4c4c4c'>")
                .add("DZPoll Bot responses from user: " + user.getFirstName() + " " + user.getLastName())
                .add("</p>")
                .add("<table style='text-align: center; color: #0079b8;'>");

        steps.forEach(step -> {
            String response = responses.stream().filter(r -> r.getStep().getId() == step.getId()).findFirst().orElseThrow().getResponse();
            htmlContent
                    .add("<tr>")
                    .add("<td style='padding: 10px; border: 1px solid #dddddd;'>")
                    .add(step.getId() + ". " + step.getMessage())
                    .add("<br>")
                    .add(response)
                    .add("</td>")
                    .add("</tr>");
                });
        htmlContent.add("</table>");
        return htmlContent.toString();
    }

}
