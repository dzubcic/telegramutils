package dz.telegramutils.bot.service;

import dz.telegramutils.bot.model.User;
import dz.telegramutils.bot.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    User createUser(long chatId, String firstName, String lastName){
        User user = new User();
        user.setChatId(chatId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return userRepository.save(user);
    }

    User getUser(long chatId) {
        return userRepository.findById(chatId).orElse(null);
    }

    void clearUser(User user) {
        userRepository.delete(user);
    }
}
