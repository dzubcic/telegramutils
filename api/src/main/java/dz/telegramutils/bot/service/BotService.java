package dz.telegramutils.bot.service;

import dz.telegramutils.bot.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.List;

@Service
public class BotService {
    private final Logger logger = LoggerFactory.getLogger(BotService.class);

    private final InfoService infoService;
    private final UserService userService;
    private final StepService stepService;
    private final MailService mailService;
    private final WorkflowService workflowService;
    private final ResponseService responseService;

    public BotService(InfoService infoService, UserService userService, StepService stepService, MailService mailService, WorkflowService workflowService, ResponseService responseService) {
        this.infoService = infoService;
        this.userService = userService;
        this.stepService = stepService;
        this.mailService = mailService;
        this.workflowService = workflowService;
        this.responseService = responseService;
    }

    public List<String> resolveUpdate(Message message) {
        logger.info("Resolving update: {}", message);
        String initMessage = infoService.getBotInformation(BotInformation.INIT_MESSAGE);
        User user = userService.getUser(message.getChat().getId());

        if (initMessage == null) {
            return List.of("Bot not initialized, please try again later!");
        }
        if (user == null || message.getText().equals("/start")) {
            return resolveFirstStep(user, message, initMessage);
        }

        List<Step> steps = stepService.getAll();
        Workflow workflow = workflowService.getWorkflowForUser(user);
        if (workflow.getStep().getId() == steps.size()) {
            return resolveLastStep(user, workflow, message.getText(), steps);
        }
        else {
            return resolveNextStep(user, workflow, message.getText());
        }

    }

    private List<String> resolveFirstStep(User user, Message message, String initMessage) {
        if (user != null) {
            clearUserData(user);
        }
        if (!message.getText().equals("/start")) {
            return List.of("Unknown command, type /start to start bot!");
        }
        user = userService.createUser(message.getChat().getId(), message.getChat().getFirstName(), message.getChat().getLastName());
        Step step = stepService.getStep(1);
        workflowService.createWorkflow(user, step);
        return List.of(initMessage, step.getMessage());
    }

    private List<String> resolveNextStep(User user, Workflow workflow, String message) {
        Step nextStep = stepService.getStep(workflow.getStep().getId() + 1);
        responseService.saveResponse(user, workflow.getStep(), message);
        workflowService.updateWorkflow(user, nextStep);
        return List.of(nextStep.getMessage());
    }

    private List<String> resolveLastStep(User user, Workflow workflow, String message, List<Step> steps) {
        responseService.saveResponse(user, workflow.getStep(), message);
        String recipient = infoService.getBotInformation(BotInformation.EMAIL);
        List<Response> responses = responseService.getAllForUser(user);
        mailService.sendResponseEmail(user, recipient, responses, steps);
        clearUserData(user);
        String endMessage = infoService.getBotInformation(BotInformation.END_MESSAGE);
        return List.of(endMessage);
    }

    private void clearUserData(User user) {
        logger.info("Clearing user data: {}", user);
        workflowService.clearForUser(user);
        responseService.clearForUser(user);
        userService.clearUser(user);
    }

}
