package dz.telegramutils.bot.service;

import dz.telegramutils.bot.model.Response;
import dz.telegramutils.bot.model.Step;
import dz.telegramutils.bot.model.User;
import dz.telegramutils.bot.repository.ResponseRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResponseService {

    private final ResponseRepository responseRepository;

    public ResponseService(ResponseRepository responseRepository) {
        this.responseRepository = responseRepository;
    }

    void saveResponse(User user, Step step, String message) {
        Response response = new Response();
        response.setUser(user);
        response.setStep(step);
        response.setResponse(message);
        responseRepository.save(response);
    }

    void clearForUser(User user) {
        responseRepository.deleteAllByUser(user);
    }

    List<Response> getAllForUser(User user) {
        return responseRepository.findAllByUser(user);
    }

}
