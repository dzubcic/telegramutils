package dz.telegramutils.bot.service;

import dz.telegramutils.bot.model.Step;
import dz.telegramutils.bot.repository.StepRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StepService {

    private final StepRepository stepRepository;

    public StepService(StepRepository stepRepository) {
        this.stepRepository = stepRepository;
    }

    public void saveSteps(List<String> steps) {
        stepRepository.deleteAll();
        for (int i = 0; i < steps.size(); i++) {
            stepRepository.save(new Step(i + 1, steps.get(i)));
        }
    }

    public List<Step> getAll() {
        return stepRepository.findAll();
    }

    Step getStep(long id) {
        return stepRepository.findById(id).orElse(null);
    }

}
