package dz.telegramutils.bot.service;

import dz.telegramutils.bot.model.Step;
import dz.telegramutils.bot.model.User;
import dz.telegramutils.bot.model.Workflow;
import dz.telegramutils.bot.repository.WorkflowRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class WorkflowService {

    private final WorkflowRepository workflowRepository;

    public WorkflowService(WorkflowRepository workflowRepository) {
        this.workflowRepository = workflowRepository;
    }

    void createWorkflow(User user, Step step) {
        Workflow workflow = new Workflow();
        workflow.setUser(user);
        workflow.setStep(step);
        workflowRepository.save(workflow);
    }

    @Transactional
    void updateWorkflow(User user, Step step) {
        Workflow workflow = getWorkflowForUser(user);
        workflow.setStep(step);
        workflowRepository.save(workflow);
    }

    Workflow getWorkflowForUser(User user) {
        return workflowRepository.findByUser(user);
    }

    void clearForUser(User user) {
        workflowRepository.deleteByUser(user);
    }

}
