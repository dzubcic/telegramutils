package dz.telegramutils.bot.service;

import dz.telegramutils.bot.model.BotConfiguration;
import dz.telegramutils.bot.model.BotInformation;
import dz.telegramutils.bot.model.Info;
import dz.telegramutils.bot.repository.InfoRepository;
import org.springframework.stereotype.Service;

@Service
public class InfoService {

    private final InfoRepository infoRepository;

    public InfoService(InfoRepository infoRepository) {
        this.infoRepository = infoRepository;
    }

    public void saveBotInformation(BotConfiguration botInformation) {
        infoRepository.save(new Info(BotInformation.INIT_MESSAGE, botInformation.getInitMessage()));
        infoRepository.save(new Info(BotInformation.END_MESSAGE, botInformation.getEndMessage()));
        infoRepository.save(new Info(BotInformation.EMAIL, botInformation.getEmail()));
    }

    public String getBotInformation(BotInformation infoProperty) {
        Info property = infoRepository.findByName(infoProperty);
        if (property == null) return null;
        return property.getValue();
    }

}
