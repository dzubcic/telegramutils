package dz.telegramutils.model.data;

import dz.telegramutils.dto.ContactFilterTypes;
import dz.telegramutils.dto.ContactFilters;
import dz.telegramutils.dto.TelegramUser;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class GenerateDevData {

    public static List<TelegramUser> getTelegramUsers() {
        List<TelegramUser> telegramUsers = new ArrayList<>();
        for (int i = 0; i < firstNames.size(); i++) {
            String firstName = firstNames.get(ThreadLocalRandom.current().nextInt(0, firstNames.size()));
            String lastName = lastNames.get(ThreadLocalRandom.current().nextInt(0, lastNames.size()));
            String phoneNumber = "3859" + ThreadLocalRandom.current().nextInt(9999999, 99999999);
            String profilePhoto = null;
            if (i < 32) {
                profilePhoto = "image/" + (i + 1) + ".jpg";
            }
            telegramUsers.add(new TelegramUser(i, firstName, lastName, phoneNumber, profilePhoto));
        }
        Collections.shuffle(telegramUsers, new Random());
        return telegramUsers;
    }

    public static List<ContactFilters> getContactFilters() {
        List<ContactFilters> contactFilters = new ArrayList<>();
        ContactFilters groupFilters = new ContactFilters(ContactFilterTypes.GROUP);
        ContactFilters superGroupFilters = new ContactFilters(ContactFilterTypes.SUPERGROUP);
        ContactFilters channelFilters = new ContactFilters(ContactFilterTypes.CHANNEL);

        addFilters(groupFilters, 19);
        addFilters(superGroupFilters, 7);
        addFilters(channelFilters, 12);

        contactFilters.add(groupFilters);
        contactFilters.add(superGroupFilters);
        contactFilters.add(channelFilters);
        return contactFilters;
    }

    private static void addFilters(ContactFilters contactFilters, int size) {
        for(int i = 0; i < size; i++) {
            String title = words.get(ThreadLocalRandom.current().nextInt(0, words.size()));
            Set<Integer> ids = new HashSet<>();
            for (int j = 0; j < ThreadLocalRandom.current().nextInt(10, 28); j++) {
                ids.add(ThreadLocalRandom.current().nextInt(0, firstNames.size()));
            }
            contactFilters.getFilters().add(new ContactFilters.Filter(title, new ArrayList<>(ids)));
        }
    }

    private static final List<String> words = List.of(
            "Blandit Limited",
            "Nulla Eu Neque Company",
            "Phasellus Dolor Corp.",
            "Nec Cursus Inc.",
            "Quisque Associates",
            "Eros Nec Tellus LLP",
            "Integer In Corp.",
            "Ut Corporation",
            "Augue Industries",
            "Sit Corp.",
            "Lectus Corporation",
            "Semper Foundation",
            "Nonummy Incorporated",
            "Widooie",
            "Fabro",
            "College",
            "Abohar",
            "Trollhättan",
            "Glovertown",
            "Leamington",
            "Castelló",
            "Wansin",
            "Beawar",
            "Edmonton",
            "Halisahar",
            "Oxford County",
            "Paraguay",
            "Pitcairn Islands",
            "Mayotte",
            "Austria",
            "Sint Maarten",
            "Macao",
            "Senegal",
            "Christmas Island",
            "Albania",
            "Angola",
            "Namibia",
            "American Samoa",
            "Italy",
            "Latvia",
            "Dominican Republic",
            "Aruba",
            "Estonia",
            "Kiribati",
            "Belize",
            "Iceland",
            "Guinea-Bissau",
            "Åland Islands",
            "Eritrea",
            "Macao"
    );

    private static final List<String> firstNames = List.of(
            "Allegra",
            "Virginia",
            "Linda",
            "Sylvia",
            "Derek",
            "Sara",
            "Coby",
            "Palmer",
            "Jerome",
            "Drake",
            "Brynne",
            "Edward",
            "Cody",
            "Malcolm",
            "Lisandra",
            "Halee",
            "Victor",
            "Haviva",
            "Peter",
            "Venus",
            "Caldwell",
            "Phoebe",
            "Valentine",
            "Bo",
            "Glenna",
            "Salvador",
            "Amy",
            "Barry",
            "Orson",
            "Hunter",
            "Jason",
            "Scott",
            "Keane",
            "Lunea",
            "Lani",
            "Bert",
            "Zeph",
            "Hedley",
            "Ali",
            "Daphne"
    );

    private static final List<String> lastNames = List.of(
            "Christensen",
            "Bryan",
            "Fitzpatrick",
            "Vargas",
            "Bell",
            "Ortega",
            "Jackson",
            "Jordan",
            "Solis",
            "Taylor",
            "Vargas",
            "Wagner",
            "Sweeney",
            "French",
            "Knapp",
            "Cardenas",
            "Keith",
            "Little",
            "Hatfield",
            "Whitfield",
            "Frazier",
            "Haynes",
            "Thompson",
            "Benton",
            "Pitts",
            "Wilson",
            "Cole",
            "Kirkland",
            "Callahan",
            "Nunez",
            "Compton",
            "Garner",
            "Noble",
            "Conley",
            "Yang",
            "Harrison",
            "Bradshaw",
            "Dickson",
            "Wooten",
            "Lynch"
    );

}
