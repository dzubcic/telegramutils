package dz.telegramutils.controllers;

import dz.telegramutils.dto.ErrorResponse;
import dz.telegramutils.dto.TelegramException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandlingAdvice {
    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandlingAdvice.class);

    @ExceptionHandler(TelegramException.class)
    public ResponseEntity<ErrorResponse> handleTelegramException(TelegramException ex) {
        logger.error("Telegram error occurred: {}", ex.getMessage());
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("TELEGRAM-SERVICE-ERROR", "true");
        return new ResponseEntity<>(new ErrorResponse(ex.getCode(), ex.getMessage()), httpHeaders, HttpStatus.BAD_REQUEST);
    }

}
