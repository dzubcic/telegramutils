package dz.telegramutils.controllers;

import dz.telegramutils.dto.ContactFilters;
import dz.telegramutils.dto.TelegramUser;
import dz.telegramutils.service.ContactsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/contacts")
public class ContactsController {
    private static final Logger logger = LoggerFactory.getLogger(ContactsController.class);

    private final ContactsService contactsService;

    public ContactsController(ContactsService contactsService) {
        this.contactsService = contactsService;
    }

    @GetMapping
    public ResponseEntity<List<TelegramUser>> getAllContacts(String sessionId) {
        logger.info("Getting all user contacts");
        List<TelegramUser> contacts = contactsService.getAllContacts(sessionId);
        logger.info("Found {} user contacts: {}", contacts.size(), contacts);
        return new ResponseEntity<>(contacts, HttpStatus.OK);
    }

    @GetMapping("/contactFilters/{currentUserId}")
    public List<ContactFilters> getContactFilters(@RequestParam String sessionId, @PathVariable Integer currentUserId) {
        logger.info("Getting user contact filters");
        return contactsService.getContactFilters(sessionId, currentUserId);
    }

}
