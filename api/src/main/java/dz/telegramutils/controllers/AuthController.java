package dz.telegramutils.controllers;

import dz.telegramutils.dto.SessionResponse;
import dz.telegramutils.dto.TelegramUser;
import dz.telegramutils.service.AuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.IOException;

@RestController
@RequestMapping("/auth")
public class AuthController {
    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @GetMapping("/checkPhoneNumber/{phoneNumber}")
    public ResponseEntity<SessionResponse> checkPhoneNumber(@PathVariable String phoneNumber, HttpSession session) throws IOException {
        logger.info("Checking Telegram user phone number {}", phoneNumber);
        SessionResponse sessionId = this.authService.checkPhoneNumber(phoneNumber, session.getId());
        return new ResponseEntity<>(sessionId, HttpStatus.OK);
    }

    @GetMapping("/checkAuthCode/{authCode}")
    public ResponseEntity<TelegramUser> checkAuthCode(@RequestParam String sessionId, @PathVariable String authCode) {
        logger.info("Checking Telegram auth code {}", authCode);
        TelegramUser currentUser = this.authService.checkAuthCode(sessionId, authCode);
        logger.info("Telegram user auth success {}", currentUser);
        return new ResponseEntity<>(currentUser, HttpStatus.OK);
    }

    @GetMapping("/checkLoginExists")
    public ResponseEntity<TelegramUser> checkExistingLogin(@RequestParam String sessionId) {
        logger.info("Checking existing user login {}", sessionId);
        TelegramUser currentUser = this.authService.getCurrentUser(sessionId);
        logger.info("Telegram existing user auth success {}", currentUser);
        return new ResponseEntity<>(currentUser, HttpStatus.OK);
    }

    @GetMapping("/logout")
    public void logout(@RequestParam String sessionId) throws IOException {
        logger.info("Logging out user session {}", sessionId);
        this.authService.logout(sessionId);
    }

}
