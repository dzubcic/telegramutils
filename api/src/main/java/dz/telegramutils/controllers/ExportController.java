package dz.telegramutils.controllers;

import dz.telegramutils.service.ExportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.Charset;
import java.util.Set;

@RestController
@RequestMapping("/export")
public class ExportController {
    private static final Logger logger = LoggerFactory.getLogger(ExportController.class);
    private static final Charset EXPORT_CHARSET = Charset.forName("windows-1252");

    private final ExportService exportService;

    public ExportController(ExportService exportService) {
        this.exportService = exportService;
    }

    @GetMapping("/csv")
    public ResponseEntity<ByteArrayResource> exportCSV(@RequestParam String sessionId, @RequestParam Set<Integer> contactIds) {
        logger.info("Exporting user contacts to CSV {}", contactIds);

        final String contactsCSV = exportService.exportCSV(sessionId, contactIds);
        final byte[] data = contactsCSV.getBytes(EXPORT_CHARSET);
        final ByteArrayResource resource = new ByteArrayResource(data);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=contacts.csv")
                .contentType(new MediaType("text", "csv", EXPORT_CHARSET))
                .contentLength(data.length)
                .body(resource);
    }

    @GetMapping("/vcard")
    public ResponseEntity<ByteArrayResource> exportVCARD(@RequestParam String sessionId, @RequestParam Set<Integer> contactIds) {
        logger.info("Exporting user contacts to VCARD {}", contactIds);

        final String contactsCSV = exportService.exportVCARD(sessionId, contactIds);
        final byte[] data = contactsCSV.getBytes(EXPORT_CHARSET);
        final ByteArrayResource resource = new ByteArrayResource(data);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=contacts.vcf")
                .contentType(new MediaType("text", "vcard", EXPORT_CHARSET))
                .contentLength(data.length)
                .body(resource);
    }

}
