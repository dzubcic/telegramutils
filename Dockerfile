FROM adoptopenjdk/openjdk12:latest

WORKDIR /

COPY api/build/libs/api-1.0-SNAPSHOT.jar /release.jar

COPY libtdjni.so /libtdjni.so

EXPOSE 8080

CMD ["java", "-jar", "-Djava.library.path=.", "release.jar"]