import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from './core/services/notification-service.service';
import { LoginService } from './core/services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  successMsg: string;
  errorMsg: string;

  constructor(private loginService: LoginService, private translate: TranslateService, private notificationService: NotificationService) {
    translate.setDefaultLang('en');
  }

  ngOnInit(): void {
    this.loginService.checkExistingLogin();

    this.notificationService.getSuccessMessageNotifications().subscribe(msg => {
      if (msg === '') {
        this.onClose();
        return;
      }
      this.successMsg = 'alerts.' + msg;
      this.errorMsg = '';
    });

    this.notificationService.getErrorMessageNotifications().subscribe(error => {
      if (error.code === 400 || error.code === 401) {
        this.errorMsg = 'errors.' + error.message;
      }
      else {
        this.errorMsg = error.message;
      }
      this.successMsg = '';
    });
  }

  onClose() {
    this.successMsg = '';
    this.errorMsg = '';
  }

}
