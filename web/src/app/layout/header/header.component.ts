import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../core/models/user.model';
import { Language, languages, StateStorageService } from '../../core/services/state-storage.service';
import { LoginService } from '../../core/services/login.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.css']
})
export class HeaderComponent implements OnInit {
  currentLanguage$: Observable<Language>;
  currentUser$: Observable<User>;
  isLoggedIn$: Observable<boolean>;
  languages: Language[] = languages;

  constructor(private stateStorageService: StateStorageService, private loginService: LoginService) {}

  ngOnInit(): void {
    this.currentLanguage$ = this.stateStorageService.getLanguage();
    this.currentUser$ = this.stateStorageService.getLoggedUser();
    this.isLoggedIn$ = this.stateStorageService.getLoggedIn();
  }

  useLanguage(language: Language) {
    this.stateStorageService.setLanguage(language);
  }

  logout() {
    this.loginService.logout(true);
  }

}
