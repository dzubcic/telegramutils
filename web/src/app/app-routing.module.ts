import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./main/login/login.component";
import { Contacts } from './main/contacts/contacts.component';
import { BotComponent } from './main/bot/bot.component';
import { AuthGuard } from './core/infrastructure/auth.guard';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'contacts', component: Contacts, canActivate: [AuthGuard] },
  { path: 'bot', component: BotComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
