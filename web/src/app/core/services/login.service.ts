import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { StateStorageService } from './state-storage.service';
import { User } from '../models/user.model';
import { Router } from '@angular/router';
import { NotificationService } from './notification-service.service';

export function getSessionId() {
  return localStorage.getItem('sessionId');
}

export function getPhoneNumber() {
  return sessionStorage.getItem('phoneNumber');
}

export interface SessionResponse {
  sessionId: string;
}

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  controllerName = '/auth';
  CHECK_PHONE_NUMBER_URL = `${environment.backendUrl}${this.controllerName}/checkPhoneNumber`;
  CHECK_AUTH_CODE_URL = `${environment.backendUrl}${this.controllerName}/checkAuthCode`;
  CHECK_LOGIN_URL = `${environment.backendUrl}${this.controllerName}/checkLoginExists`;
  LOGOUT_URL = `${environment.backendUrl}${this.controllerName}/logout`;

  constructor(private http: HttpClient, private stateStorageService: StateStorageService, private router: Router, private notificationService: NotificationService) { }

  checkPhoneNumber(phoneNumber: string) {
    this.http.get<SessionResponse>(this.CHECK_PHONE_NUMBER_URL + '/' + phoneNumber).subscribe(response => {
      this.stateStorageService.setLoading(false);
      localStorage.setItem("sessionId", response.sessionId);
      sessionStorage.setItem("phoneNumber", phoneNumber);
      this.stateStorageService.setAuthFlow(2);
      this.notificationService.publishSuccessMessage('');
    });
  }

  checkAuthCode(authCode: string) {
    this.http.get<User>(this.CHECK_AUTH_CODE_URL + '/' + authCode, {params: {sessionId: getSessionId()}}).subscribe(response => {
      this.stateStorageService.setLoading(false);
      this.stateStorageService.setLoggedUser(response);
      this.stateStorageService.setAuthFlow(3);
      this.notificationService.publishSuccessMessage('loginSuccess');
    });
  }

  checkExistingLogin() {
    if (getSessionId()) {
      if (getPhoneNumber()) {
        this.stateStorageService.setAuthFlow(2);
      }
      else {
        this.http.get<User>(this.CHECK_LOGIN_URL, {params: {sessionId: getSessionId()}}).subscribe(response => {
          this.stateStorageService.setLoggedUser(response);
          this.router.navigate(['/contacts']);
          this.notificationService.publishSuccessMessage('loginSuccess');
        }, error => {
          this.notificationService.publishErrorMessage({message: "Unauthorized", code: 401});
          this.logout(false);
          localStorage.clear();
        });
      }
    }
  }

  logout(publishError: boolean) {
    this.http.get(this.LOGOUT_URL, {params: {sessionId: getSessionId()}}).subscribe(() => {});
    this.stateStorageService.clearLoggedUser();
    this.router.navigate(['']);
    if (publishError) {
      this.notificationService.publishSuccessMessage('logoutSuccess');
    }
  }

}
