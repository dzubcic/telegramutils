import { Injectable } from '@angular/core';
import { StateStorageService } from './state-storage.service';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';
import { environment } from '../../../environments/environment';
import { getSessionId } from './login.service';
import { ContactFilters } from '../models/contact-filters.model';
import { NotificationService } from './notification-service.service';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  controllerName = '/contacts';
  GET_CONTACTS_URL = `${environment.backendUrl}${this.controllerName}`;
  GET_CONTACT_FILTERS_URL = `${environment.backendUrl}${this.controllerName}/contactFilters`;

  constructor(private stateStorageService: StateStorageService, private http: HttpClient, private notificationService: NotificationService) { }

  public getAllContacts() {
    this.http.get<User[]>(this.GET_CONTACTS_URL, {params: {sessionId: getSessionId()}}).subscribe(response => {
      this.stateStorageService.setContacts(response);
    });
  }

  public getContactFilters() {
    const currentUserId = this.stateStorageService.getLoggedUserId();
    this.http.get<ContactFilters[]>(this.GET_CONTACT_FILTERS_URL + '/' + currentUserId, {params: {sessionId: getSessionId()}}).subscribe(response => {
      this.stateStorageService.setContactFilters(response);
      this.notificationService.publishSuccessMessage('');
    });
  }

}
