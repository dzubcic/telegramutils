import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { getSessionId } from './login.service';
import { NotificationService } from './notification-service.service';

@Injectable({
  providedIn: 'root'
})
export class ExportService {
  controllerName = '/export';
  EXPORT_CSV_URL = `${environment.backendUrl}${this.controllerName}/csv`;
  EXPORT_VCARD_URL = `${environment.backendUrl}${this.controllerName}/vcard`;

  constructor(private notificationService: NotificationService) { }

  public exportCSV(contactIds: number[]) {
    this.notificationService.publishSuccessMessage('exportSuccess');
    window.location.href = `${this.EXPORT_CSV_URL}?sessionId=${getSessionId()}&contactIds=${contactIds.join(",")}`;
  }

  public exportVCARD(contactIds: number[]) {
    this.notificationService.publishSuccessMessage('exportSuccess');
    window.location.href = `${this.EXPORT_VCARD_URL}?sessionId=${getSessionId()}&contactIds=${contactIds.join(",")}`;
  }

}
