import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../models/user.model';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { ContactFilters } from '../models/contact-filters.model';
import { BotConfiguration } from '../models/bot.model';

export interface Language {
  code: string;
  name: string;
}

export const hrvatski: Language =  {
  code: 'hr',
  name: 'Hrvatski'
};

export const english: Language =  {
  code: 'en',
  name: 'English'
};

export const languages: Language[] = [
  hrvatski, english
];

@Injectable({
  providedIn: 'root'
})
export class StateStorageService {
  private readonly initUser: User = {
    id: -1,
    firstName: 'anonymous',
    lastName: '',
    phoneNumber: '',
    profilePhoto: null
  };

  private readonly loggedUser$: BehaviorSubject<User> = new BehaviorSubject<User>(this.initUser);
  private readonly language$: BehaviorSubject<Language> = new BehaviorSubject<Language>(english);
  private readonly authFlow$: BehaviorSubject<number> = new BehaviorSubject<number>(1);
  private readonly contacts$: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);
  private readonly loading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private readonly botConfiguration$: BehaviorSubject<BotConfiguration> = new BehaviorSubject<BotConfiguration>(null);
  private readonly contactFilters$: BehaviorSubject<ContactFilters[]> = new BehaviorSubject<ContactFilters[]>([]);

  constructor(private translateService: TranslateService) { }

  public isLoading() {
    return this.loading$.asObservable();
  }

  public setLoading(loading: boolean) {
    this.loading$.next(loading);
  }

  public getBotConfiguration() {
    return this.botConfiguration$.asObservable();
  }

  public setBotConfiguration(botConfiguration: BotConfiguration) {
    this.botConfiguration$.next(botConfiguration);
  }

  public setContactFilters(contactFilters: ContactFilters[]) {
    this.contactFilters$.next(contactFilters);
  }

  public getContactFilters() {
    return this.contactFilters$.asObservable();
  }

  public getAuthFlow() {
    return this.authFlow$.asObservable();
  }

  public getLanguage() {
    return this.language$.asObservable();
  }

  public setLanguage(language: Language) {
    this.translateService.use(language.code);
    this.language$.next(language);
  }

  public getLoggedIn(): Observable<boolean> {
    return this.loggedUser$.pipe(map(value => value.id !== -1));
  }

  public isLoggedIn() {
    const value = this.loggedUser$.getValue();
    return value.id !== -1;
  }

  public getLoggedUserId() {
    return this.loggedUser$.getValue().id;
  }

  public getLoggedUser() {
    return this.loggedUser$.asObservable();
  }

  public setAuthFlow(step: number) {
    this.authFlow$.next(step);
  }

  public setLoggedUser(user: User) {
    sessionStorage.clear();
    this.loggedUser$.next(user);
  }

  public clearLoggedUser() {
    localStorage.clear();
    this.loggedUser$.next(this.initUser);
  }

  public getContacts() {
    return this.contacts$.asObservable();
  }

  public setContacts(contacts: User[]) {
    this.contacts$.next(contacts);
  }
}
