import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StateStorageService } from './state-storage.service';
import { BotConfiguration } from '../models/bot.model';
import { environment } from '../../../environments/environment';
import { NotificationService } from './notification-service.service';

@Injectable({
  providedIn: 'root'
})
export class BotService {
  controllerName = '/bot';
  BOT_URL = `${environment.backendUrl}${this.controllerName}`;

  constructor(private http: HttpClient, private stateStorageService: StateStorageService, private notificationService: NotificationService) { }

  public configureBot(botConfiguration: BotConfiguration) {
    this.http.post(this.BOT_URL, botConfiguration).subscribe(() => {
      this.notificationService.publishSuccessMessage('configurationSuccess')
    });
  }

  public getConfiguration() {
    this.http.get<BotConfiguration>(this.BOT_URL).subscribe(resp => {
      this.stateStorageService.setBotConfiguration(resp);
    });
  }

}
