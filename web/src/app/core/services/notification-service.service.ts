import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ErrorResponse } from '../infrastructure/error-interceptor';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private readonly successPublisher$: Subject<string> = new Subject<string>();
  private readonly errorPublisher$: Subject<ErrorResponse> = new Subject<ErrorResponse>();

  constructor() { }


  public publishSuccessMessage(msg: string) {
    this.successPublisher$.next(msg);
  }

  public getSuccessMessageNotifications(): Observable<string> {
    return this.successPublisher$.asObservable();
  }

  public publishErrorMessage(msg: ErrorResponse) {
    this.errorPublisher$.next(msg);
  }

  public getErrorMessageNotifications(): Observable<ErrorResponse> {
    return this.errorPublisher$.asObservable();
  }
}
