import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { NotificationService } from '../services/notification-service.service';
import { StateStorageService } from '../services/state-storage.service';

export class ErrorResponse {
  code: number;
  message: string;
}

@Injectable({ providedIn: 'root' })
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private notificationService: NotificationService, private stateStorageService: StateStorageService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError(error => {
        if (error instanceof HttpErrorResponse) {
          this.stateStorageService.setLoading(false);
          const httpErrorResponse: HttpErrorResponse = error as HttpErrorResponse;
          if (httpErrorResponse.headers.has("TELEGRAM-SERVICE-ERROR")) {
            const errorResponse = httpErrorResponse.error as ErrorResponse;
            this.notificationService.publishErrorMessage(errorResponse);
          }
        }
        return throwError(error);
      }) as any);
  }

}
