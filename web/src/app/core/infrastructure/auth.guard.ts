import { Injectable } from '@angular/core';
import { StateStorageService } from '../services/state-storage.service';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private stateStorageService: StateStorageService) {}

  canActivate(): boolean {
    if (!this.stateStorageService.isLoggedIn()) {
      this.router.navigate(['']);
      return false;
    }
    return true;
  }

}
