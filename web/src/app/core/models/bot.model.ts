export interface BotConfiguration {
  initMessage: string;
  endMessage: string;
  email: string;
  botSteps: string[];
}
