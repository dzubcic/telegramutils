export interface ContactFilters {
  filterType: string;
  filters: Filter[];
}

export interface Filter {
  title: string;
  memberIds: number[];
}
