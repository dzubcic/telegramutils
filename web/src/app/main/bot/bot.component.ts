import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { BotService } from '../../core/services/bot.service';
import { StateStorageService } from '../../core/services/state-storage.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-bot',
  templateUrl: './bot.component.html',
  styleUrls: ['./bot.component.css']
})
export class BotComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  initMessage: FormControl = new FormControl('', Validators.required);
  endMessage: FormControl = new FormControl('', Validators.required);
  email: FormControl = new FormControl('', [Validators.required, Validators.email]);
  botSteps: FormArray = new FormArray([new FormControl('', Validators.required)]);

  botConfiguration: FormGroup = new FormGroup({
    initMessage: this.initMessage,
    endMessage: this.endMessage,
    email: this.email,
    botSteps: this.botSteps
  });

  constructor(private botService: BotService, private stateStorageService: StateStorageService) { }

  ngOnInit() {
    this.botService.getConfiguration();
    this.subscription = this.stateStorageService.getBotConfiguration().subscribe(config => {
      if (config !== null && config.botSteps.length > 0) {
        this.initMessage.patchValue(config.initMessage);
        this.endMessage.patchValue(config.endMessage);
        this.email.patchValue(config.email);
        while (this.botSteps.length !== 0) {
          this.botSteps.removeAt(0);
        }
        config.botSteps.forEach(s => {
          this.botSteps.push(new FormControl(s, Validators.required));
        });
      }
    });
  }

  submit() {
    this.botService.configureBot(this.botConfiguration.value);
  }

  addStep() {
    this.botSteps.push(new FormControl('', Validators.required))
  }

  removeStep() {
    if (this.botSteps.length === 1) return;
    this.botSteps.removeAt(this.botSteps.controls.length - 1);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
