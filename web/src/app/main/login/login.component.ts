import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { getPhoneNumber, LoginService } from '../../core/services/login.service';
import { StateStorageService } from '../../core/services/state-storage.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  phoneNumber: FormControl = new FormControl('', Validators.required);
  authCode: FormControl = new FormControl({value: '', disabled: true}, Validators.required);
  loginGroup = new FormGroup({
    phoneNumber: this.phoneNumber,
    authCode: this.authCode
  });
  authStep: boolean;
  loading$: Observable<boolean>;

  constructor(private loginService: LoginService, private stateStorageService: StateStorageService, private router: Router) { }

  ngOnInit() {
    this.loading$ = this.stateStorageService.isLoading();
    if (getPhoneNumber()) {
      this.phoneNumber.patchValue(getPhoneNumber());
    }

    if (this.stateStorageService.isLoggedIn()) {
      this.router.navigate(['/contacts']);
    }
    this.stateStorageService.getAuthFlow().subscribe(step => {
     if (step === 1) {
       this.reset();
     }
     else if (step === 2) {
       this.authStep = true;
       this.authCode.enable();
       this.phoneNumber.disable();
     }
     else {
       this.router.navigate(['/contacts'])
     }
    });
  }

  submit() {
    if (this.authStep) {
      this.stateStorageService.setLoading(true);
      this.loginService.checkAuthCode(this.authCode.value);
    }
    else {
      this.stateStorageService.setLoading(true);
      this.loginService.checkPhoneNumber(this.phoneNumber.value);
    }
  }

  back() {
    sessionStorage.clear();
    this.reset();
  }

  reset() {
    this.phoneNumber.enable();
    this.authCode.disable();
    this.loginGroup.reset();
    this.authStep = false;
  }

}
