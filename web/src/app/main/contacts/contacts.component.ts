import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { ContactsService } from '../../core/services/contacts.service';
import { StateStorageService } from '../../core/services/state-storage.service';
import { User } from '../../core/models/user.model';
import { Observable, Subscription } from 'rxjs';
import { ContactFilters } from '../../core/models/contact-filters.model';
import { ExportService } from '../../core/services/export.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class Contacts implements OnInit, OnDestroy {
  contacts: User[];
  subscription: Subscription;
  contactFilters$: Observable<ContactFilters[]>;
  selectedContacts: number[] = [];
  filteredContacts: number[] = [];
  scrollShow: boolean;

  constructor(private contactsService: ContactsService, private stateStorageService: StateStorageService, private exportService: ExportService) { }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    this.scrollShow = window.pageYOffset > 200;
  }

  ngOnInit() {
    this.contactsService.getAllContacts();
    this.contactsService.getContactFilters();
    this.contactFilters$ = this.stateStorageService.getContactFilters();
    this.subscription = this.stateStorageService.getContacts().subscribe(resp => {
      this.contacts = resp;
    });
  }

  selectContact(contactId: number) {
    if (this.selectedContacts.indexOf(contactId) === -1) {
      this.selectedContacts.push(contactId);
    }
    else {
      this.selectedContacts = this.selectedContacts.filter(c => c !== contactId);
    }
  }

  selectAllContacts() {
    this.selectedContacts = this.filter().map(c => c.id);
  }

  filterClicked(ids: number[]) {
    this.selectedContacts = [];
    this.filteredContacts = ids;
  }

  exportCSV() {
    if (this.selectedContacts.length === 0) return;
    this.exportService.exportCSV(this.selectedContacts);
    this.selectedContacts = [];
  }

  exportVCARD() {
    if (this.selectedContacts.length === 0) return;
    this.exportService.exportVCARD(this.selectedContacts);
    this.selectedContacts = [];
  }

  filter() {
    if (this.filteredContacts.length === 0) return this.contacts;
    return this.contacts.filter(c => this.filteredContacts.indexOf(c.id) !== -1);
  }

  isSelected(contactId: number) {
    return this.selectedContacts.indexOf(contactId) !== -1;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  toTop() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }
}
