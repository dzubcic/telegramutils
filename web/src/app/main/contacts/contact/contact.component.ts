import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../../../core/models/user.model';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {
  @Input() contact: User;
  @Input() selected: boolean;
  @Output() selectedContact = new EventEmitter<number>();

  constructor() {}

}
