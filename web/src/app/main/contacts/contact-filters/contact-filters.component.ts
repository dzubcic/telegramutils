import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ContactFilters, Filter } from '../../../core/models/contact-filters.model';

@Component({
  selector: 'app-contact-filters',
  templateUrl: './contact-filters.component.html',
  styleUrls: ['./contact-filters.component.css']
})
export class ContactFiltersComponent {
  @Input() filters: ContactFilters[];
  @Input() contactsSize: number;
  @Input() selectedSize: number;
  @Output() selectAll = new EventEmitter();
  @Output() filterContacts = new EventEmitter<number[]>();
  @Output() exportCSV = new EventEmitter();
  @Output() exportVCARD = new EventEmitter();
  activeFilter: string;
  isExpanded: boolean[] = [false, false, false];

  constructor() { }

  emitFilters(filter: Filter, index: number) {
    this.activeFilter = filter.title + index;
    this.filterContacts.emit(filter.memberIds);
  }

  reset() {
    this.filterContacts.emit([]);
    this.activeFilter = '';
    for (let i = 0; i < this.isExpanded.length; i++) {
      this.isExpanded[i] = false;
    }
  }

}
