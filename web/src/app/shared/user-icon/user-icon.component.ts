import { Component, Input } from '@angular/core';
import { User } from '../../core/models/user.model';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-user-icon',
  templateUrl: './user-icon.component.html',
  styleUrls: ['./user-icon.component.css']
})
export class UserIconComponent {
  @Input() user: User;
  backendUrl: string = environment.backendUrl + '/';
  color: string = "hsla(" + (Math.random() * 360) + ", 100%, 50%, 1)";

  constructor() { }

}
